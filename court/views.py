from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.paginator import Paginator
from django.db.models import Q
from taggit.models import Tag

from . import models
from .forms import  GuestCommentForm, SearchForm


def main (request):
    arts=models.Articles.objects.get_queryset().order_by('-date')
    paginator=Paginator(arts,7)
    if 'page' in request.GET:
        page_num=request.GET['page']
    else:
        page_num=1
    page=paginator.get_page(page_num)
    if 'keyword' in request.GET:
        keyword=request.GET['keyword']
        form=SearchForm(request.GET)
        errors=form.run_my_validator(keyword)
        if errors or keyword == '':
            messages.warning(request, 'Поле не должно содержать больше двух пробелов или быть пустым')
            return redirect('court:main')
        else:
            if form.is_valid():
                keyword=form.cleaned_data['keyword']
                return redirect ('court:search',keyword) 
    else:
        form=SearchForm()
    context={'arts':page.object_list,'page':page,'form':form }
    return render (request,"main.html",context)

def arts_by_tag(request,tag_slug=None):
    arts=models.Articles.objects.all()
    tag=None
    if tag_slug:
        tag=get_object_or_404(Tag,slug=tag_slug)
        arts_by_tag=arts.filter(tags__in=[tag])
    context={'arts':arts_by_tag,'tag':tag}
    return render(request,'arts_by_tag.html',context)
    

def detail (request, pk, slug):
    art=models.Articles.objects.get(pk=pk)
    comments=models.Comment.objects.filter(art=pk,is_active=True)
    paginator=Paginator(comments,10,orphans=1)
    slug=slug
    if 'page' in request.GET:
        page_num=request.GET['page']
    else:
        page_num=1
    page=paginator.get_page(page_num)
    form_class=GuestCommentForm
    form=form_class()
    if request.method=="POST":
        c_form=form_class(request.POST)
        if c_form.is_valid():
            new_comment=c_form.save(commit=False)
            new_comment.art_id=pk
            new_comment.save()
            messages.add_message(request,messages.SUCCESS,'Комментарий добавлен')
            return HttpResponseRedirect(request.path)
        else:
            form=c_form
            messages.add_message(request,messages.WARNING,'Комментарий НЕ добавлен')   
    context={'art':art,'comments':page.object_list, 'form':form,'page':page}
    return render(request,'detail.html',context)

def search (request,keyword):
    keyword=keyword
    arts=models.Articles.objects.all()
    q=Q(title__icontains=keyword)|Q(body__icontains=keyword)|Q(tags__name__icontains=keyword)
    arts=arts.filter(q).distinct()
    paginator=Paginator(arts,7)
    if 'page' in request.GET:
        page_num=request.GET['page']
    else:
        page_num=1
    page=paginator.get_page(page_num)
    context={'page':page,'arts':page.object_list,'keyword':keyword}
    return render(request,'search.html',context)

