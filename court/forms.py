from django import forms
from django.core import validators
from django.core.exceptions import ValidationError
from captcha.fields import CaptchaField

from .models import *


class GuestCommentForm(forms.ModelForm):
    captcha=CaptchaField(label="Введите текст с картинки")
    class Meta:
        model=Comment
        fields=('author','content','captcha',)

ins_validator=validators.RegexValidator(regex=r'\s{2,}',message="Поле не должно содержать больше двух пробелов", code='invalid_keyword',inverse_match=True)
class SearchForm(forms.Form):
    keyword=forms.CharField(required=False,max_length=20, label='Поиск по названию, содержанию, тегу',min_length=4,validators=[ins_validator])
    default_validators=[ins_validator]
    
    def run_my_validator (self, keyword):
        errors=[]
        try:
            ins_validator(keyword)
        except ValidationError:
            errors=[ins_validator.message]
            self.errors.update({ins_validator.code:ins_validator.message})
        return errors
    
    
              
        
        