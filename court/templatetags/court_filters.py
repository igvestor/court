from django import template
from easy_thumbnails.files import get_thumbnailer
import re
register = template.Library()

@register.filter
def replace(value,arg):
    return value.replace(arg,"***")



@register.filter
def cutDL(text):
    """Отрезает из текста слова Донекая (Луганская) Народная Республика и производные в разных падежах"""
    reg=r"(г[.])? ((Донецк|Луганск)\w*)?\s?(Народн\w*)?\s?(Республик\w*)*[,]?"
    text_cutted=re.sub(reg,' ',text)
    return re.sub('\s+',' ',text_cutted).strip().strip(',')

@register.filter
def comment_filter(count):
    if count in (1,21,31,41,51,61,71,81,91,101,):
        return f'{count} комментарий'
    elif count in (2,3,4,22,23,24,32,33,34,42,43,44,52,53,54,62,63,64,72,73,74,82,83,84,92,93,94,):
        return f'{count} комментария'
    else:
        return f'{count} комментариев'


@register.filter
def image_exist(img):
    return bool(img) 



