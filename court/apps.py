from django.apps import AppConfig
from django.dispatch import Signal

user_registered=Signal (providing_args=['instance'])

class CourtConfig(AppConfig):
    name = 'court'
    verbose_name="Верховный суд"
