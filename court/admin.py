from django.contrib import admin

from . import models

class ArticlesAdmin(admin.ModelAdmin):
    model=models.Articles
    prepopulated_fields = {"slug": ("title",),}


admin.site.register (models.Articles,ArticlesAdmin)
admin.site.register (models.AdmUser)
admin.site.register (models.Comment)


# Register your models here.
