from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db.models.fields import SlugField
from django.utils.text import slugify
from taggit.managers import TaggableManager
from .utilities import get_timestamp_path
from .templatetags.court_filters import cutDL
import supreme_court.settings as set


class AdmUser (AbstractUser):
    pass

class Articles (models.Model):
    title=models.CharField (max_length=300)
    author=models.ForeignKey(
        AdmUser,
        on_delete=models.CASCADE,
    )
    body=models.TextField()
    date=models.DateTimeField(auto_now_add=True)
    image=models.ImageField(blank=True,upload_to="media/")
    slug=SlugField(max_length=300,blank=True,allow_unicode=True,unique=True)
    tags = TaggableManager(blank=True)
    
    def __str__(self):
        return self.title
       
    def slug_it(self):
        if not self.slug:
            self.slug=slugify(cutDL(self.title),allow_unicode=True)
            self.save(update_fields=['slug'])
        return self.slug

        class Meta:
            ordering=['-date']

class Comment (models.Model):
    art=models.ForeignKey(Articles,on_delete=models.CASCADE,verbose_name="Объявление")
    author=models.CharField(max_length=30, verbose_name="Автор")
    content=models.TextField(verbose_name="Содержание")
    is_active=models.BooleanField(default=True,db_index=True,verbose_name="Выводить на экран?")
    date=models.DateTimeField(auto_now_add=True,db_index=True,verbose_name="Опубликован")

    def __str__(self):
        return self.content

    class Meta:
        verbose_name_plural="Комментарии"
        verbose_name="Комментарий"
        ordering=['date']






