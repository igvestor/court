"""
WSGI config for supreme_court project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os, locale, sys

from django.core.wsgi import get_wsgi_application
from whitenoise import WhiteNoise



os.environ ['DJANGO_SETTINGS_MODULE']='supreme_court.settings'

application = get_wsgi_application()
application = WhiteNoise(application)
# application.add_files('/media/', prefix='media/')


locale.setlocale(locale.LC_ALL,'ru')

