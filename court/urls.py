from django.urls import path,  re_path
from .views import *
import supreme_court.settings as s

app_name='court'
urlpatterns=[
    path('', main,name='main'),
    re_path(r'(?P<pk>[0-9]+)_(?P<slug>[-a-zA-Zа-яA-Я0-9_]+)/$', detail,name='detail'),
    path('tags/<slug:tag_slug>/',arts_by_tag, name='arts_by_tag'),
    path('search_for=<str:keyword>', search, name='search'),
] 
